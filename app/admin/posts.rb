ActiveAdmin.register Post do
  permit_params :user_id, :title, :body

  form do |f|
    f.inputs do
      f.input :user_id, :label => 'User', :as => :select, :collection => User.all.map{|u| [u.name, u.id]}
      f.input :title
      f.input :body, as: :action_text
    end
    f.actions
  end
end
